import json
import random
import sys
from datetime import datetime

from pyqtgraph.Qt import QtGui
from PyQt6.QtWidgets import QWidget, QMainWindow, QGridLayout
from PyQt6.QtWidgets import QCheckBox, QInputDialog, QMenu
from PyQt6.QtGui import QAction, QCursor

from mapinit import MapBuild


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.setWindowTitle('Star chart logger')
        self.UiComponents()
        self.show()

    def UiComponents(self):

        def _createMenuBar():

            menubar = self.menuBar()
            fileMenu = menubar.addMenu('File')
            editMenu = menubar.addMenu('Edit')

            loadAct = QAction('Load config', self)
            saveAct = QAction('Save config', self)
            exitAct = QAction('Exit', self)

            newAct = QAction('Clear Markers', self)
            colorInvertAct = QAction('Invert map colors', self)

            fileMenu.addAction(loadAct)
            fileMenu.addAction(saveAct)
            fileMenu.addAction(exitAct)
            editMenu.addAction(newAct)
            editMenu.addAction(colorInvertAct)

            newAct.triggered.connect(newFile)
            loadAct.triggered.connect(loadFile)
            saveAct.triggered.connect(saveFile)
            exitAct.triggered.connect(exitFile)
            colorInvertAct.triggered.connect(invertColors)

        def newFile():
            mapscatter.plot.removeItem(mapscatter.user_scatter)
            mapscatter.user_config_data = []
            mapscatter.user_list = []
            mapscatter.user_scatter.clear()
            mapscatter.plot.addItem(mapscatter.user_scatter)

        def loadFile():
            configname = QtGui.QFileDialog.getOpenFileName(
                self, 'Open File', filter='*.json')
            points = mapscatter.userObjects(configname[0])

            for point in points:
                x = point['x']
                y = point['y']
                name = point['Name']
                place = point['Place']
                record_date = point['Date']
                new_point_data = {
                                "x": x,
                                "y": y,
                                "Name": name,
                                "Place": place,
                                "Date": record_date
                            }

                point_details = {
                    'pos': (x, y),
                    'data': f'\
                        Name: {name}\nDate: {record_date}\nPlace: {place}',
                    'size': 12,
                    'symbol': 't'
                    }
                mapscatter.user_list.append(point_details)
                mapscatter.user_config_data.append(new_point_data)

            mapscatter.user_scatter.setData(mapscatter.user_list)

        def saveFile():
            config_id = random.getrandbits(16)
            configname = QtGui.QFileDialog.getSaveFileName(
                self, 'Save File', filter='*.json')

            data = {
                "ID": config_id,
                "Name": configname[0],
                "Markers": mapscatter.user_config_data
            }

            with open(configname[0], "w") as write_file:
                json.dump(data, write_file, ensure_ascii=False, indent=4)
            print(configname, "Saved!")

        def exitFile():
            sys.exit('User exit')

        def invertColors():
            if mapscatter.bgr:
                mapscatter.plot.setBackground('k')
                mapscatter.bgr = False
            else:
                mapscatter.plot.setBackground('w')
                mapscatter.bgr = True

        def pointsToMap(self, scatter):
            if self.isChecked():
                print('Checked')
                scatter.setPointsVisible(visible=True)
            else:
                print('Unchecked')
                scatter.setPointsVisible(visible=False)

        def mouseClicked(event):
            mb = event.button()
            coords = mapscatter.scatter.mapFromScene(event.scenePos())
            x = coords.x()
            y = coords.y()

            if str(mb) == "MouseButton.RightButton":
                popMenu = QMenu(self)
                addAction = QAction('Add Marker', self)
                popMenu.addAction(addAction)
                cursor = QCursor()
                popMenu.popup(cursor.pos())
                addAction.triggered.connect(lambda: addMarker(x, y))

        def addMarker(x, y):
            name, ok = QInputDialog.getText(
                self, 'Add marker', f'X coordinate: {x} \nY coordinate: {y}')
            place, ok2 = QInputDialog.getText(
                self, 'Add marker', 'Enter place of observation')
            if not place:
                place = None

            if ok:
                record_date = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
                new_point_data = {
                    "x": x,
                    "y": y,
                    "Name": name,
                    "Place": place,
                    "Date": record_date
                }

                mapscatter.user_config_data.append(new_point_data)
                new_point = {
                    'pos': (x, y),
                    'data': f'\
                        Name: {name}\nDate: {record_date}\nPlace: {place}',
                    'size': 12,
                    'symbol': 't'
                }
                mapscatter.user_list.append(new_point)
                mapscatter.user_scatter.setData(mapscatter.user_list)

        _createMenuBar()
        mapscatter = MapBuild()
        mapscatter.basePlotBuild()

        central_widget = QWidget()
        layout_top = QGridLayout()
        central_widget.setLayout(layout_top)

        m_chkbox = QCheckBox('Messier objects', self)
        u_chkbox = QCheckBox('My markers', self)
        u_chkbox.setChecked(True)
        m_chkbox.stateChanged.connect(
            lambda: pointsToMap(m_chkbox, mapscatter.m_scatter))
        u_chkbox.stateChanged.connect(
            lambda: pointsToMap(u_chkbox, mapscatter.user_scatter))

        mapscatter.plot.scene().sigMouseClicked.connect(mouseClicked)

        layout_top.addWidget(m_chkbox, 0, 0)
        layout_top.addWidget(u_chkbox, 0, 1)
        layout_top.setColumnStretch(2, 1)
        layout_top.addWidget(mapscatter.plot, 2, 0, 1, 3)
        self.setCentralWidget(central_widget)
