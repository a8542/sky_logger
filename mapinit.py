import pyqtgraph as pg

import json


class MapBuild():

    def basePlotBuild(self):
        self.plot = pg.plot()
        self.plot.setBackground('w')
        self.bgr = True
        self.plot.showGrid(x=True, y=True)
        self.plot.getViewBox().invertX(True)
        self.plot.setMenuEnabled(False)
        self.plot.enableAutoRange('xy', True)
        self.vbox = self.plot.getViewBox()

        self.scatter = pg.ScatterPlotItem(
            size=5,
            pen=pg.mkPen(169, 169, 169, 255),
            brush=pg.mkBrush(0, 0, 0, 255)
            )
        self.m_scatter = pg.ScatterPlotItem(
            size=5,
            brush=pg.mkBrush(30, 255, 35, 255),
            hoverable=True,
            tip='x: {x:.6g}\ny: {y:.6g}\n{data}'.format
            )
        self.n_scatter = pg.ScatterPlotItem(
            size=5,
            brush=pg.mkBrush(30, 255, 35, 255)
            )
        self.user_scatter = pg.ScatterPlotItem(
            size=5,
            pen=pg.mkPen('dark slate gray', width=1.2),
            brush=pg.mkBrush(0, 206, 209, 182),
            hoverable=True,
            tip='x: {x:.6g}\ny: {y:.6g}\n{data}'.format
            )

        self.stars = self.starFinalCoord('stardata.csv')
        self.m_objects = self.skyObjects(
            'messier_coordinates_degree.csv', None, 'r', None, None)
        self.user_config_data = []
        self.user_list = []

        self.scatter.addPoints(self.stars)
        self.m_scatter.setData(self.m_objects)
        self.plot.addItem(self.scatter)
        self.plot.addItem(self.user_scatter)
        self.plot.addItem(self.m_scatter)
        self.m_scatter.setPointsVisible(visible=False)

    def starFinalCoord(self, filename):
        coordinates_list = []
        with open(filename, mode='rt') as f:
            for line in f:
                line = line.strip()
                current_line = line.split(',')
                x = current_line[0]
                y = current_line[1]
                mag = float(current_line[2])
                size = self._mag_to_d(mag)
                if 0.5 <= size <= 10:
                    star_details = {
                        'pos': (x, y),
                        'size': size * 1.5,
                        'symbol': None
                        }
                    coordinates_list.append(star_details)

            return coordinates_list

    def skyObjects(self, filename, size, pen, brush, symbol):
        object_coordinates_list = []
        with open(filename, mode='rt') as f:
            for line in f:
                current_line = line.split(',')
                x = current_line[0]
                y = current_line[1]
                mag = float(current_line[2])
                size = self._mag_to_d(mag)
                object_name = current_line[3]
                type = current_line[4].strip()
                type_dict = {
                            "Oc": {
                                "Name":  "Open Cluster",
                                "Symbol": "o",
                                "Color": "red",
                            },
                            "Gc": {
                                "Name":  "Globular Cluster",
                                "Symbol": "o",
                                "Color": "orange red",
                            },
                            "Di": {
                                "Name":  "Diffuse Nebulae",
                                "Symbol": "h",
                                "Color": "deep pink",
                            },
                            "Sn": {
                                "Name":  "Supernova remnant",
                                "Symbol": "d",
                                "Color": "deep pink",
                            },
                            "Pl": {
                                "Name":  "Planetary Nebulae",
                                "Symbol": "h",
                                "Color": "deep pink",
                            },
                            "Sp": {
                                "Name":  "Spiral Galaxy",
                                "Symbol": "o",
                                "Color": "indigo",
                            },
                            "El": {
                                "Name":  "Elliptical Galaxy",
                                "Symbol": "o",
                                "Color": "indigo",
                            },
                            "Ba": {
                                "Name":  "Barred Galaxy",
                                "Symbol": "o",
                                "Color": "indigo",
                            },
                            "Ln": {
                                "Name":  "Lenticular Galaxy",
                                "Symbol": "o",
                                "Color": "indigo",
                            },
                            "Ir": {
                                "Name":  "Irregular Galaxy",
                                "Symbol": "o",
                                "Color": "indigo",
                            },
                            "As": {
                                "Name":  "Asterism",
                                "Symbol": "d",
                                "Color": "medium spring green",
                            },
                            "Ds": {
                                "Name":  "Double Star",
                                "Symbol": "star",
                                "Color": "medium spring green",
                            },
                            "MW": {
                                "Name":  "Milky Way Patch",
                                "Symbol": "d",
                                "Color": "medium spring green",
                            }
                }
                type_anno = type_dict[type]["Name"]
                symbol = type_dict[type]["Symbol"]
                pen = type_dict[type]["Color"]

                object_details = {
                    'pos': (x, y),
                    'data': f'Name: {object_name}\nType: {type_anno}',
                    'size': size + 7,
                    'pen': pen,
                    'brush': brush,
                    'symbol': symbol
                    }
                object_coordinates_list.append(object_details)

        return object_coordinates_list

    def userObjects(self, filename):

        with open(filename) as json_file:
            data = json.load(json_file)
            markers = data['Markers']

        return markers

    def _mag_to_d(self, m):
        mag_range = DIMMEST_MAG - BRIGHTEST_MAG
        m_score = (DIMMEST_MAG - m) / mag_range
        r_range = MAX_D - MIN_D
        return MIN_D + m_score * r_range


MIN_D = 1
MAX_D = 4

DIMMEST_MAG = 6
BRIGHTEST_MAG = -1.5
