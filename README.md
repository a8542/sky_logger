## Description

Sky logger - is interactive sky map which allows you to add markers for the sky objects you already observed in telescope and track you progress.
Once i started to practice telescope observations i found myself lacking good tool to track my progress visualy, apps i found on the internet were only allowing to have text logger with observations and i'm more of visual person. So i remembered movie "Contact" by Robert Zemeckis where Dr. Ellie Arroway added pins to sky map on the wall tracking stars she already checked. Sky logger is basically the same thing. It shows map of stars for both poles (from 0 to 90 degrees and 0 24 hours) where you can add you own markers, save configurations and load them.


## Requirments

Sky logger requires Python3 and PyQt6 for GIU. Maps are built with PyQtGraph library and use ScatterPlotItem to plot stars and markers.

Install required packages with:

```
pip -r requirements.txt
```

## Install and Run

Sky logger doesn't require installation. To start app run:

> python3 main.py


## Usage

Once app is running you will see sky map with stars with 0 to 7 magnitude. You can zoom in/out map to see stars closer.
To ADD marker right clik on any point on the map to call context menu with option to "Add Marker". You will see input dialog to add name and place for your marker (date, time will be added automatically), enter your details and hit "Ok" button - this will add gree triangle marker on the point. Hovering on catalog objects or user markers will show information on the object.
To SAVE your progress go to File - Save and enter name of config, this will create .json file with marker coordinates. This file can be moved between apps.
To LOAD previously saved config go to File - Load and chose .json file, it will load all saved points. Loaded filed will add saved points to current once.

**Messier objects** checkbox will show or hide all objects from Messier catalog.
**My markers** checkbox will show or hide user added markers.
**Clear markers** under Edit menu will remove all previously added markers from map.
**Inverted map colors** under Edit menu - will revert colors on background from white to black.


## Credits

To build star map i used some formulas and functions from project Star Charts: https://github.com/codebox/star-charts