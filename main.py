from PyQt6.QtWidgets import QApplication
import sys  # We need sys so that we can pass argv to QApplication
import uigraph


def main():
    app = QApplication([])
    app.setStyle('Fusion')
    main_window = uigraph.MainWindow()
    main_window.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
